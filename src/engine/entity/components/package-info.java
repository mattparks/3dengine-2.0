/**
 * Provides various components that are useful in virtually any game situation.
 */
package engine.entity.components;