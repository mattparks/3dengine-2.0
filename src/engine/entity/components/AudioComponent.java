package engine.entity.components;

import java.util.HashMap;
import java.util.Map;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.sound.Sound;
import engine.toolbox.IDAssigner;

/**
 * Holds sounds that can be played by an entity.
 */
public class AudioComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private Map<String, Sound> sounds;
	private float innerBounds;
	private float outerBounds;

	/**
	 * Creates a new AudioComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param innerBounds The radius from the entity where the sound will be at default audio.
	 * @param outerBounds The radius from the {@code innerBounds} where the sound will diffuse to no volume.
	 * @param soundNames Strings identifying the sounds being passed in.
	 * @param soundFiles The sounds being added to the component.
	 */
	public AudioComponent(Entity entity, float innerBounds, float outerBounds, String[] soundNames, Sound[] soundFiles) {
		super(entity, ID);
		this.innerBounds = innerBounds;
		this.outerBounds = outerBounds;
		sounds = new HashMap<>();

		for (int i = 0; i < soundNames.length; i++) {
			sounds.put(soundNames[i], soundFiles[i]);
		}
	}

	/**
	 * Gets the volume that the sound was originally created with.
	 *
	 * @param soundName The sound of interest.
	 * @return default volume if sound exists, -1.0 otherwise.
	 */
	public float getDefaultVolume(String soundName) {
		Sound sound = sounds.get(soundName);

		if (sound != null) {
			return sound.getDefaultVolume();
		}

		return -1.0f;
	}

	/**
	 * Sets the volume of a sound to a particular value.
	 *
	 * @param soundName The sound of interest.
	 * @param volume How loud the audio should be played at. 1.0 specifies normal volume, and lower or higher values specify quieter or louder volumes, respectively.
	 */
	public void setVolume(String soundName, float volume) {
		Sound sound = sounds.get(soundName);

		if (sound != null) {
			sound.setVolume(volume);
		}
	}

	public float getInnerBounds() {
		return innerBounds;
	}

	public void setInnerBounds(float innerBounds) {
		this.innerBounds = innerBounds;
	}

	public float getOuterBounds() {
		return outerBounds;
	}

	public void setOuterBounds(float outerBounds) {
		this.outerBounds = outerBounds;
	}

	/**
	 * Plays a sound. If it is already playing, it is stopped and restarted. When the audio is finished playing, it will stop if the object's shouldLoop is false, or it will restart if the object's shouldLoop is true.
	 *
	 * @param soundName The sound to be played.
	 */
	public void play(String soundName) {
		Sound sound = sounds.get(soundName);

		if (sound != null) {
			sound.play();
		}
	}

	/**
	 * Stops playing this sound, but leaves its position alone. When the object is played again, it should start where it left off.
	 *
	 * @param soundName The sound to be paused.
	 */
	public void pause(String soundName) {
		Sound sound = sounds.get(soundName);

		if (sound != null) {
			sound.pause();
		}
	}

	/**
	 * Stops playing this sound and resets its position to the start of the audio. When the object is played again, it should start at the beginning.
	 *
	 * @param soundName The sound to be stopped.
	 */
	public void stop(String soundName) {
		Sound sound = sounds.get(soundName);

		if (sound != null) {
			sound.stop();
		}
	}

	@Override
	public void update(double delta) {
	}
}
