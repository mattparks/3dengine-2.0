package game.entities;

import java.io.IOException;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.components.AABBComponent;
import engine.entity.components.ColliderComponent;
import engine.entity.components.CollisionComponent;
import engine.entity.components.ModelComponent;
import engine.factory.TexturedModelFactory;
import engine.models.TexturedModel;
import engine.space.ISpatialStructure;

public class EntityTreePine extends Entity {
	public EntityTreePine(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale) throws IOException {
		super(structure, position, rotation, scale, null);
		new AABBComponent(this);
		new ColliderComponent(this);
		new CollisionComponent(this);
		TexturedModel texturedModel = TexturedModelFactory.get("pine", "pine");
		new ModelComponent(this, texturedModel, 0);
	}
}
