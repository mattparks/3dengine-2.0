package engine.devices;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

/**
 * An Display Device implemented using LWJGL.
 */
public class DeviceDisplay {
	private static final String TITLE = "Game Testing";
	private static final int START_WIDTH = 1280;
	private static final int START_HEIGHT = 720;
	private static final int AA_QUALITY = 4; // Highest 8
	private static final int FPS_CAP = 60;
	private static final boolean VSYNC = true;

	private static boolean isFullscreen;
	private static long lastFrameTime;
	private static float delta;

	public DeviceDisplay() {
		isFullscreen = false;
		setFullscreen(isFullscreen);

		// try {
		// ByteBuffer[] icons = new ByteBuffer[1];
		// icons[0] = Loader.loadIcons("res/icons/icon16x16.png", 16, 16);
		// Display.setIcon(icons);
		// } catch (IOException ex) {
		// ex.printStackTrace();
		// }

		// Alpha Bits, Depth Bits, Stencil Bits, and Samples
		PixelFormat pFormat = new PixelFormat(AA_QUALITY, AA_QUALITY, AA_QUALITY, AA_QUALITY);

		try {
			Display.create(pFormat, new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true));
		} catch (LWJGLException e) {
			System.err.println("Could not create the display device!");
			e.printStackTrace();
		}

		getCurrentTime();
	}

	/**
	 * Updates the display every frame, this is used to clear and render the screen but also updates game frame times.
	 */
	public static void updateDisplay() {
		GL11.glViewport(0, 0, getDisplayWidth(), getDisplayHeight());
		Display.sync(FPS_CAP);
		Display.update();

		long currentFrameTime = getCurrentTime();
		delta = (currentFrameTime - lastFrameTime) / 1000f;
		lastFrameTime = currentFrameTime;
	}

	/**
	 * Takes a screenshot of the current state in the display and saves it into a png or jpg image.
	 */
	public static void takeScreenshot() {
		GL11.glReadBuffer(GL11.GL_FRONT);
		int bpp = 4;

		// Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
		ByteBuffer buffer = BufferUtils.createByteBuffer(getDisplayWidth() * getDisplayHeight() * bpp);
		GL11.glReadPixels(0, 0, getDisplayWidth(), getDisplayHeight(), GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

		String name = Calendar.getInstance().get(Calendar.MONTH) + 1 + "." + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "." + Calendar.getInstance().get(Calendar.HOUR) + "." + Calendar.getInstance().get(Calendar.MINUTE) + "." + (Calendar.getInstance().get(Calendar.SECOND) + 1);
		File saveDirectory = new File("screenshots");

		if (!saveDirectory.exists()) {
			try {
				saveDirectory.mkdir();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}

		File file = new File(saveDirectory + "/" + name + ".png"); // The file to save the pixels too.
		String format = "png"; // "PNG" or "JPG".
		BufferedImage image = new BufferedImage(getDisplayWidth(), getDisplayHeight(), BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < getDisplayWidth(); x++) {
			for (int y = 0; y < getDisplayHeight(); y++) {
				int i = (x + getDisplayWidth() * y) * bpp;
				int r = buffer.get(i) & 0xFF;
				int g = buffer.get(i + 1) & 0xFF;
				int b = buffer.get(i + 2) & 0xFF;
				image.setRGB(x, getDisplayHeight() - (y + 1), 0xFF << 24 | r << 16 | g << 8 | b);
			}
		}

		try {
			ImageIO.write(image, format, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the display to windowed or fullscreen.
	 *
	 * @param fullscreen Should the window be in fullscreen or windowed mode?
	 */
	public static void setFullscreen(boolean fullscreen) {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();

		int displayWidth = gd.getDisplayMode().getWidth();
		int displayHeight = gd.getDisplayMode().getHeight();

		try {
			DisplayMode targetDisplayMode = null;

			if (fullscreen) {
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				int freq = 0;

				for (DisplayMode current : modes) {
					if (current.getWidth() == displayWidth && current.getHeight() == displayHeight) {
						if (targetDisplayMode == null || current.getFrequency() >= freq) {
							if (targetDisplayMode == null || current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel()) {
								targetDisplayMode = current;
								freq = targetDisplayMode.getFrequency();
							}
						}

						// If its found a match for bpp and frequence against the original display mode then it's probably best to go for this one since it's most likely compatible with the monitor.
						if (current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel() && current.getFrequency() == Display.getDesktopDisplayMode().getFrequency()) {
							targetDisplayMode = current;
							break;
						}
					}
				}
			} else {
				targetDisplayMode = new DisplayMode(START_WIDTH, START_HEIGHT);
			}

			if (targetDisplayMode == null) {
				System.err.println("Failed to find display mode: " + displayWidth + "x" + displayHeight);
				return;
			} else {
				Display.setTitle(TITLE);
				Display.setDisplayMode(targetDisplayMode);
				Display.setResizable(!fullscreen);
				Display.setFullscreen(fullscreen);
				Display.setVSyncEnabled(VSYNC);
				isFullscreen = fullscreen;
			}
		} catch (LWJGLException e) {
			System.err.println("Unable to setup display mode " + displayWidth + "x" + displayHeight);
			e.printStackTrace();
		}
	}

	/**
	 * @return Returns the width of the display in pixels.
	 */
	public static int getDisplayWidth() {
		return Display.getWidth();
	}

	/**
	 * @return Returns the height of the display in pixels.
	 */
	public static int getDisplayHeight() {
		return Display.getHeight();
	}

	/**
	 * @return Returns the aspect ratio between the displays width and height.
	 */
	public static float getDisplayRatio() {
		return getDisplayWidth() * 1f / (getDisplayHeight() * 1f);
	}

	/**
	 * @return Returns weather the window is fullscreen.
	 */
	public static boolean isFullscreen() {
		return isFullscreen;
	}

	/**
	 * @return Gets the how many milliseconds passed sense the last frame.
	 */
	public static float getFrameTimeSeconds() {
		return delta;
	}

	/**
	 * @return Gets the current system time.
	 */
	public static long getCurrentTime() {
		return Sys.getTime() * 1000 / Sys.getTimerResolution();
	}

	/**
	 * @return Returns memory stats in a string.
	 */
	public static StringBuilder getUsedMemory() {
		Runtime runtime = Runtime.getRuntime();
		NumberFormat format = NumberFormat.getInstance();

		StringBuilder sb = new StringBuilder();
		long maxMemory = runtime.maxMemory();
		long allocatedMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();

		sb.append("Free memory: " + format.format(freeMemory / 1024) + "<br/>");
		sb.append("Allocated memory: " + format.format(allocatedMemory / 1024) + "<br/>");
		sb.append("Max memory: " + format.format(maxMemory / 1024) + "<br/>");
		sb.append("Total free memory: " + format.format((freeMemory + maxMemory - allocatedMemory) / 1024) + "<br/>");
		return sb;
	}

	/**
	 * Releases all resources being used and closes the display. This display device should not be used after this is called.
	 */
	public static void cleanUp() {
		Display.destroy();
	}
}
