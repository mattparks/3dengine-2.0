package engine.sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import engine.devices.DeviceAudio;

/**
 * Represents audio data for a particular audio device.
 */
public class SoundData {
	private int id;

	/**
	 * Creates a new SoundData object. It is preferred to use this object instead of manipulating an {@link DeviceAudio}'s audio data methods directly.
	 *
	 * @param fileName The name and path to the sound file to be loaded.
	 */
	public SoundData(String fileName) {
		id = 0;

		try {
			id = loadAudio(fileName);
		} catch (IOException e) {
			e.printStackTrace(); // If the audio file cannot be loaded and sent to the {@code device}.
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		dispose();
		super.finalize();
	}

	/**
	 * Releases this object. This should be called when the object will no longer be used, and no methods or fields should be used after this method is called.
	 */
	public void dispose() {
		id = DeviceAudio.releaseAudioData(id);
	}

	/**
	 * Gets the audio device's integer id for this audio data.
	 *
	 * @return The audio device's integer id for this audio data.
	 */
	public int getId() {
		return id;
	}

	private static int loadAudio(String fileName) throws UnsupportedAudioFileException, IOException {
		int result = -1;

		try (AudioInputStream stream = AudioSystem.getAudioInputStream(new File(fileName))) {
			AudioFormat streamFormat = stream.getFormat();
			result = DeviceAudio.createAudioData(readStream(stream), getFormat(streamFormat), (int) streamFormat.getSampleRate(), streamFormat.isBigEndian());
		}

		return result;
	}

	private static int getFormat(AudioFormat streamFormat) {
		int format = -1;

		if (streamFormat.getChannels() == 1) {
			if (streamFormat.getSampleSizeInBits() == 8) {
				format = DeviceAudio.FORMAT_MONO_8;
			} else if (streamFormat.getSampleSizeInBits() == 16) {
				format = DeviceAudio.FORMAT_MONO_16;
			}
		} else if (streamFormat.getChannels() == 2) {
			if (streamFormat.getSampleSizeInBits() == 8) {
				format = DeviceAudio.FORMAT_STEREO_8;
			} else if (streamFormat.getSampleSizeInBits() == 16) {
				format = DeviceAudio.FORMAT_STEREO_16;
			}
		}

		return format;
	}

	private static byte[] readStream(AudioInputStream stream) throws IOException {
		byte[] data = new byte[stream.available()];
		int bytesRead = 0;
		int totalBytesRead = 0;

		while ((bytesRead = stream.read(data, totalBytesRead, data.length - totalBytesRead)) != -1 && totalBytesRead < data.length) {
			totalBytesRead += bytesRead;
		}

		return data;
	}
}
