package engine.space;

import org.lwjgl.util.vector.Vector3f;

/**
 * A 3D axis-aligned bounding box.
 */
public class AABB {
	private final float minX, minY, minZ;
	private final float maxX, maxY, maxZ;

	public AABB() {
		this(new Vector3f(0, 0, 0), new Vector3f(0, 0, 0));
	}

	public AABB(AABB aabb) {
		this(new Vector3f(aabb.getMinX(), aabb.getMinY(), aabb.getMinZ()), new Vector3f(aabb.getMaxX(), aabb.getMaxY(), aabb.getMaxZ()));
	}

	/**
	 * Creates a new 3d AABB based on it's extents.
	 *
	 * @param minPos The minimum extent of the box.
	 * @param maxPos The minimum extent of the box.
	 */
	public AABB(Vector3f minPos, Vector3f maxPos) {
		minX = minPos.getX();
		minY = minPos.getY();
		minZ = minPos.getZ();
		maxX = maxPos.getX();
		maxY = maxPos.getY();
		maxZ = maxPos.getZ();
	}

	/**
	 * Tests whether another AABB is completely contained by this one.
	 *
	 * @param other The AABB being tested for containment
	 * @return True if {@code other} is contained by this AABB, false otherwise.
	 */
	public boolean contains(AABB other) {
		return minX <= other.getMinX() && other.getMaxX() <= maxX && minY <= other.getMinY() && other.getMaxY() <= maxY && minZ <= other.getMinZ() && other.getMaxZ() <= maxZ;
	}

	/**
	 * Tests whether another AABB is intersecting this one.
	 *
	 * @param other The AABB being tested for intersection
	 * @return True if {@code other} is intersecting this AABB, false otherwise.
	 */
	public boolean intersects(AABB other) {
		return intersectCube(new Vector3f(other.getMinX(), other.getMinY(), other.getMinZ()), new Vector3f(other.getMaxX(), other.getMaxY(), other.getMaxZ()));
	}

	/**
	 * Tests whether this AABB intersects a cube.
	 *
	 * @param minPosition The minimum extent of the cube.
	 * @param maxPosition The maximum extent of the cube.
	 * @return True if the cube intersects this AABB, false otherwise.
	 */
	public boolean intersectCube(Vector3f minPosition, Vector3f maxPosition) {
		return minX < maxPosition.getX() && maxX > minPosition.getX() && minY < maxPosition.getY() && maxY > minPosition.getY() && minZ < maxPosition.getZ() && maxZ > minPosition.getZ();
	}

	/**
	 * Adjusts a movement amount on X so that after the move is performed, this AABB will not intersect {@code other}.
	 * <p>
	 * This method assumes that this AABB can actually intersect {@code other} after some amount of movement on x, even if it won't necessarily intersect it after the movement specified by {@code moveAmtX}.
	 *
	 * @param other The AABB that this AABB is resolving against.
	 * @param moveAmountX The amount this AABB is trying to move.
	 * @return The new, adjusted move amount that guarantees no intersection.
	 */
	public double resolveCollisionX(AABB other, double moveAmountX) {
		double newAmtX = moveAmountX;

		if (moveAmountX == 0.0) {
			return moveAmountX;
		}

		if (moveAmountX > 0) { // This max == other min
			newAmtX = other.getMinX() - maxX;
		} else { // This min == other max
			newAmtX = other.getMaxX() - minX;
		}

		if (Math.abs(newAmtX) < Math.abs(moveAmountX)) {
			moveAmountX = newAmtX;
		}

		return moveAmountX;
	}

	/**
	 * Adjusts a movement amount on Y so that after the move is performed, this AABB will not intersect {@code other}.
	 * <p>
	 * This method assumes that this AABB can actually intersect {@code other} after some amount of movement on y, even if it won't necessarily intersect it after the movement specified by {@code moveAmtY}.
	 *
	 * @param other The AABB that this AABB is resolving against.
	 * @param moveAmountY The amount this AABB is trying to move.
	 * @return The new, adjusted move amount that guarantees no intersection.
	 */
	public double resolveCollisionY(AABB other, double moveAmountY) {
		double newAmtY = moveAmountY;

		if (moveAmountY == 0.0) {
			return moveAmountY;
		}

		if (moveAmountY > 0) { // This max == other min.
			newAmtY = other.getMinY() - maxY;
		} else { // This min == other max.
			newAmtY = other.getMaxY() - minY;
		}

		if (Math.abs(newAmtY) < Math.abs(moveAmountY)) {
			moveAmountY = newAmtY;
		}

		return moveAmountY;
	}

	/**
	 * Adjusts a movement amount on Z so that after the move is performed, this AABB will not intersect {@code other}.
	 * <p>
	 * This method assumes that this AABB can actually intersect {@code other} after some amount of movement on z, even if it won't necessarily intersect it after the movement specified by {@code moveAmtZ}.
	 *
	 * @param other The AABB that this AABB is resolving against.
	 * @param moveAmountZ The amount this AABB is trying to move.
	 * @return The new, adjusted move amount that guarantees no intersection.
	 */
	public double resolveCollisionZ(AABB other, double moveAmountZ) {
		double newAmtZ = moveAmountZ;

		if (moveAmountZ == 0.0) {
			return moveAmountZ;
		}

		if (moveAmountZ > 0) { // This max == other min.
			newAmtZ = other.getMinZ() - maxZ;
		} else { // This min == other max.
			newAmtZ = other.getMaxZ() - minZ;
		}

		if (Math.abs(newAmtZ) < Math.abs(moveAmountZ)) {
			moveAmountZ = newAmtZ;
		}

		return moveAmountZ;
	}

	/**
	 * Calculates the center of this AABB on the X axis.
	 *
	 * @return The center location of this AABB on the X axis.
	 */
	public double getCenterX() {
		return (minX + maxX) / 2.0;
	}

	/**
	 * Calculates the center of this AABB on the Y axis.
	 *
	 * @return The center location of this AABB on the Y axis.
	 */
	public double getCenterY() {
		return (minY + maxY) / 2.0;
	}

	/**
	 * Calculates the center of this AABB on the Z axis.
	 *
	 * @return The center location of this AABB on the Z axis.
	 */
	public double getCenterZ() {
		return (minZ + maxZ) / 2.0;
	}

	/**
	 * Gets the width of this AABB.
	 *
	 * @return The width of this AABB.
	 */
	public double getWidth() {
		return maxX - minX;
	}

	/**
	 * Gets the height of this AABB.
	 *
	 * @return The height of this AABB.
	 */
	public double getHeight() {
		return maxY - minY;
	}

	/**
	 * Gets the depth of this AABB.
	 *
	 * @return The depth of this AABB.
	 */
	public double getDepth() {
		return maxZ - minZ;
	}

	public float getMinX() {
		return minX;
	}

	public float getMinY() {
		return minY;
	}

	public float getMinZ() {
		return minZ;
	}

	public float getMaxX() {
		return maxX;
	}

	public float getMaxY() {
		return maxY;
	}

	public float getMaxZ() {
		return maxZ;
	}

	@Override
	public String toString() {
		return "Min " + new Vector3f(minX, minY, minZ).toString() + " | Max " + new Vector3f(maxX, maxY, maxZ).toString();
	}

	/**
	 * Creates a new AABB equivalent to this, but scaled away from the origin by a certain amount.
	 *
	 * @param expand Amount to scale up the AABB.
	 * @return A new AABB, scaled by the specified amounts.
	 */
	public AABB expand(Vector3f expand) {
		return new AABB(new Vector3f(minX - expand.getX(), minY - expand.getY(), minZ - expand.getZ()), new Vector3f(maxX + expand.getX(), maxY + expand.getY(), maxZ + expand.getZ()));
	}

	public AABB scale(float scale) {
		return scale(new Vector3f(scale, scale, scale));
	}

	/**
	 * Creates a new AABB equivalent to this, scaled away from the center origin.
	 *
	 * @param scale Amount to scale up the AABB.
	 * @return A new AABB, scaled by the specified amounts.
	 */
	public AABB scale(Vector3f scale) {
		return new AABB(new Vector3f(minX * scale.getX(), minY * scale.getY(), minZ * scale.getZ()), new Vector3f(maxX * scale.getX(), maxY * scale.getY(), maxZ * scale.getZ()));
	}

	/**
	 * Creates an AABB equivalent to this, but in a new position.
	 *
	 * @param position The amount to move.
	 * @return An AABB equivalent to this, but in a new position.
	 */
	public AABB move(Vector3f position) {
		return new AABB(new Vector3f(minX + position.getX(), minY + position.getY(), minZ + position.getZ()), new Vector3f(maxX + position.getX(), maxY + position.getY(), maxZ + position.getZ()));
	}

	/**
	 * Creates an AABB that bounds both this AABB and another AABB.
	 *
	 * @param other The other AABB being bounded.
	 * @return An AABB that bounds both this AABB and {@code other}.
	 */
	public AABB combine(AABB other) {
		float newMinX = Math.min(minX, other.getMinX());
		float newMinY = Math.min(minY, other.getMinY());
		float newMinZ = Math.min(minZ, other.getMinZ());
		float newMaxX = Math.max(maxX, other.getMaxX());
		float newMaxY = Math.max(maxY, other.getMaxY());
		float newMaxZ = Math.max(maxZ, other.getMaxZ());
		return new AABB(new Vector3f(newMinX, newMinY, newMinZ), new Vector3f(newMaxX, newMaxY, newMaxZ));
	}

	/**
	 * Creates a new AABB equivalent to this, but stretched by a certain amount.
	 *
	 * @param stretch The amount to stretch.
	 * @return A new AABB, stretched by the specified amounts.
	 */
	public AABB stretch(Vector3f stretch) {
		float newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ;

		if (stretch.getX() < 0) {
			newMinX = minX + stretch.getX();
			newMaxX = maxX;
		} else {
			newMinX = minX;
			newMaxX = maxX + stretch.getX();
		}

		if (stretch.getY() < 0) {
			newMinY = minY + stretch.getY();
			newMaxY = maxY;
		} else {
			newMinY = minY;
			newMaxY = maxY + stretch.getY();
		}

		if (stretch.getZ() < 0) {
			newMinZ = minZ + stretch.getZ();
			newMaxZ = maxZ;
		} else {
			newMinZ = minZ;
			newMaxZ = maxZ + stretch.getZ();
		}

		return new AABB(new Vector3f(newMinX, newMinY, newMinZ), new Vector3f(newMaxX, newMaxY, newMaxZ));
	}
}
