package engine.entity.components;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.models.TexturedModel;
import engine.space.AABB;
import engine.toolbox.IDAssigner;

/**
 * Creates a model with a texture that can be rendered into the world.
 */
public class ModelComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private TexturedModel model;
	private final AABB modelAABB;
	private int textureIndex;

	public ModelComponent(Entity entity, TexturedModel model) {
		this(entity, model, 0);
	}

	/**
	 * Creates a new ModelComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param model The model that will be attached too this entity.
	 * @param textureIndex What texture index this entity should render from (0 default).
	 */
	public ModelComponent(Entity entity, TexturedModel model, int textureIndex) {
		super(entity, ID);
		this.model = model;
		modelAABB = model.getRawModel().getAABB();
		this.textureIndex = textureIndex;
		init();
	}

	private void init() {
		AABB gottenAABB = new AABB(modelAABB);
		gottenAABB = gottenAABB.scale(new Vector3f(super.getEntity().getScale(), super.getEntity().getScale(), super.getEntity().getScale()));
		ColliderComponent c = (ColliderComponent) getEntity().getComponent(ColliderComponent.ID);

		if (c != null) {
			c.fitAABB(gottenAABB);
		} else {
			AABBComponent aabbC = (AABBComponent) getEntity().getComponent(AABBComponent.ID);

			if (aabbC != null) {
				aabbC.setMesh(model.getRawModel());
				aabbC.fitAABB(gottenAABB);
			}
		}
	}

	public TexturedModel getModel() {
		return model;
	}

	public AABB getModelAABB() {
		return modelAABB;
	}

	/**
	 * Gets the textures coordinate offset that is used in rendering the model.
	 *
	 * @return The coordinate offset used in rendering.
	 */
	public Vector2f getTextureOffset() {
		int column = textureIndex % model.getTexture().getNumberOfRows();
		int row = textureIndex / model.getTexture().getNumberOfRows();
		return new Vector2f((float) row / (float) model.getTexture().getNumberOfRows(), (float) column / (float) model.getTexture().getNumberOfRows());
	}

	@Override
	public void update(double delta) {
	}
}
