package engine.entity.components;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.models.RawModel;
import engine.space.AABB;
import engine.toolbox.IDAssigner;

public class AABBComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private RawModel mesh;
	private AABB aabb;

	public AABBComponent(Entity entity) {
		super(entity, ID);
		aabb = new AABB();
	}

	public RawModel getMesh() {
		return mesh;
	}

	public void setMesh(RawModel mesh) {
		this.mesh = mesh;
	}

	/**
	 * Ensures the bounds of this entity are at least big enough to contain {@code newAABB}.
	 *
	 * @param newAABB The AABB this entity must be able to contain.
	 */
	public void fitAABB(AABB newAABB) {
		super.getEntity().removeFromStructure();

		if (aabb.getWidth() == 0.0 && aabb.getHeight() == 0.0) {
			aabb = newAABB;
		} else {
			aabb = aabb.combine(newAABB);
		}

		super.getEntity().addToStructure();
	}

	/**
	 * Moves an AABB into the position of this entity.
	 *
	 * @param aabb The AABB to be translated.
	 * @return An AABB translated into the position of this entity.
	 */
	public AABB translateAABB(AABB aabb) {
		return aabb.move(super.getEntity().getPosition());
	}

	public AABB getAABB() {
		return translateAABB(aabb);
	}

	@Override
	public void update(double delta) {
	}
}
