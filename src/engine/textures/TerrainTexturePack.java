package engine.textures;

public class TerrainTexturePack {
	private GameTexture backgroundTexture;
	private GameTexture rTexture;
	private GameTexture gTexture;
	private GameTexture bTexture;

	public TerrainTexturePack(GameTexture backgroundTexture, GameTexture rTexture, GameTexture gTexture, GameTexture bTexture) {
		this.backgroundTexture = backgroundTexture;
		this.rTexture = rTexture;
		this.gTexture = gTexture;
		this.bTexture = bTexture;
	}

	public GameTexture getBackgroundTexture() {
		return backgroundTexture;
	}

	public GameTexture getrTexture() {
		return rTexture;
	}

	public GameTexture getgTexture() {
		return gTexture;
	}

	public GameTexture getbTexture() {
		return bTexture;
	}
}
