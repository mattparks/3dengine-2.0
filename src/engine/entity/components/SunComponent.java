package engine.entity.components;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.toolbox.Colour;
import engine.toolbox.IDAssigner;

public class SunComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();

	public SunComponent(Entity entity) {
		super(entity, ID);
	}

	// http://pastebin.com/v0Jt57s4

	// private void calculateSunPosition(float time) {
	// double radionTime = ((time * Math.PI) / 12000) - (Math.PI / 2);
	// float x = (float) (SUN_DISTANCE * Math.cos(radionTime));
	// float z = (float) ((1 - RELATIVE_HEIGHT_OF_ARC) * SUN_DISTANCE * Math.sin(radionTime));
	// float y = (float) (RELATIVE_HEIGHT_OF_ARC * SUN_DISTANCE * Math.sin(radionTime)) + HEIGHT_OFFSET;
	// sky.getSun().setPosition(x, y, z);
	// }

	// public void setPosition(float x, float y, float z) {
	// sunlight.setPosition(x, y, z);
	// Vector3f toSunFromCamera = new Vector3f(x - camera.getX(), y - camera.getY(), z - camera.getZ());
	// toSunFromCamera.normalise();
	// this.pitch = 360 - (float) Math.toDegrees(Math.asin(toSunFromCamera.y));
	// toSunFromCamera.scale(renderDistance);
	// this.x = camera.getX() + toSunFromCamera.x;
	// this.y = camera.getY() + toSunFromCamera.y;
	// this.z = camera.getZ() + toSunFromCamera.z;
	//
	// float dX = camera.getX() - x;
	// float dZ = camera.getZ() - z;
	// this.yaw = (float) (Math.toDegrees(Math.atan2(dX, dZ)));
	// }

	public void updateColour(Colour sunColour) {
		LightComponent lc = (LightComponent) super.getEntity().getComponent(LightComponent.ID);

		if (lc != null) {
			lc.setColour(sunColour);
		}
	}

	@Override
	public void update(double delta) {
	}
}
