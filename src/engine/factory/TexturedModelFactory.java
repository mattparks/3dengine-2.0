package engine.factory;

import engine.models.RawModel;
import engine.models.TexturedModel;
import engine.textures.GameTexture;

/**
 * A factory for creating textured models.
 */
public class TexturedModelFactory {
	/**
	 * Creates a new textured Model Factory.
	 */
	public TexturedModelFactory() {
	}

	/**
	 * Gets a new object from the factory. If the desired textured model data is already loaded, then the textured model data is reused and not loaded again. Otherwise, the textured model data is loaded from the specified file.
	 *
	 * @param modelFileName The file path for model file.
	 * @param textureFileName The file path for image file.
	 * @return Returns the newly created raw model object.
	 */
	public static TexturedModel get(String modelFileName, String textureFileName) {
		RawModel model = RawModelFactory.get(modelFileName);
		GameTexture texture = TextureFactory.get(textureFileName);
		return new TexturedModel(model, texture);
	}
}
