package game.entities;

import java.io.IOException;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.components.AABBComponent;
import engine.entity.components.ColliderComponent;
import engine.entity.components.CollisionComponent;
import engine.entity.components.ModelComponent;
import engine.entity.components.PlayerComponent;
import engine.factory.TexturedModelFactory;
import engine.models.TexturedModel;
import engine.space.ISpatialStructure;

public class EntityPlayer extends Entity {
	public EntityPlayer(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, String username) throws IOException {
		super(structure, position, rotation, 1.0f, null);
		new AABBComponent(this);
		new ColliderComponent(this);
		new CollisionComponent(this);
		TexturedModel texturedModel = TexturedModelFactory.get("person", "playerTexture");
		new ModelComponent(this, texturedModel, 0);
		new PlayerComponent(this, username);
	}
}
