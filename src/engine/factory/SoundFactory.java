package engine.factory;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import engine.sound.Sound;
import engine.sound.SoundData;

/**
 * A factory for creating Sounds.
 */
public class SoundFactory {
	private static String filePath;
	private static Map<String, SoftReference<SoundData>> loaded;

	/**
	 * Creates a new Sound Factory.
	 *
	 * @param filePath The base path for sound files.
	 */
	public SoundFactory(String filePath) {
		SoundFactory.filePath = filePath;
		SoundFactory.loaded = new HashMap<>();
	}

	/**
	 * Gets a new object from the factory. If the desired sound data is already loaded, then the sound data is reused and not loaded again. Otherwise, the sound data is loaded from the specified file.
	 *
	 * @param fileName The name of an audio file to be loaded.
	 * @param volume How loud the audio should be played at. 1.0 specifies normal volume, and lower or higher values specify quieter or louder volumes, respectively.
	 * @param pitch What pitch the audio should be played at. 1.0 specifies normal pitch, and lower or higher values specify higher or lower pitches, respectively.
	 * @param shouldLoop Whether the audio should automatically restart when finished playing.
	 * @return A Sound matching the specification.
	 */
	public static Sound get(String fileName, float volume, float pitch, boolean shouldLoop) {
		fileName = filePath + fileName;
		SoftReference<SoundData> ref = loaded.get(fileName);
		SoundData data = ref == null ? null : ref.get();

		if (data == null) {
			loaded.remove(fileName);
			data = new SoundData(fileName);
			loaded.put(fileName, new SoftReference<SoundData>(data));
		}

		return new Sound(data, volume, pitch, shouldLoop);
	}

	public static String getFilePath() {
		return filePath;
	}
}
