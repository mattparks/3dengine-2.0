package engine.textures;

public class GameTexture {
	private int textureID;
	private int normalMap;

	private float shineDamper;
	private float reflectivity;

	private boolean hasTransparency;
	private boolean useFakeLighting;

	private int numberOfRows;

	public GameTexture(int textureID) {
		this(textureID, false, false, 1);
	}

	public GameTexture(int textureID, boolean hasTransparency, boolean useFakeLighting, int numberOfRows) {
		this(textureID, 0, 1, 0);
		this.hasTransparency = hasTransparency;
		this.useFakeLighting = useFakeLighting;
		this.numberOfRows = numberOfRows;
	}

	public GameTexture(int textureID, int normalMap, float shineDamper, float reflectivity) {
		this.textureID = textureID;
		this.normalMap = normalMap;
		this.shineDamper = shineDamper;
		this.reflectivity = reflectivity;
		numberOfRows = 1;
	}

	public int getTextureID() {
		return textureID;
	}

	public void setTextureID(int textureID) {
		this.textureID = textureID;
	}

	public int getNormalMap() {
		return normalMap;
	}

	public void setNormalMap(int normalMap) {
		this.normalMap = normalMap;
	}

	public float getShineDamper() {
		return shineDamper;
	}

	public void setShineDamper(float shineDamper) {
		this.shineDamper = shineDamper;
	}

	public float getReflectivity() {
		return reflectivity;
	}

	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}

	public boolean isHasTransparency() {
		return hasTransparency;
	}

	public void setHasTransparency(boolean hasTransparency) {
		this.hasTransparency = hasTransparency;
	}

	public boolean isUseFakeLighting() {
		return useFakeLighting;
	}

	public void setUseFakeLighting(boolean useFakeLighting) {
		this.useFakeLighting = useFakeLighting;
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}
}
