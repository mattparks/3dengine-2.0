package game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import engine.devices.DeviceAudio;
import engine.devices.DeviceDisplay;
import engine.entity.Camera;
import engine.entity.Entity;
import engine.entity.Light;
import engine.entity.Terrain;
import engine.entity.WaterTile;
import engine.entity.components.PlayerComponent;
import engine.factory.RawModelFactory;
import engine.factory.SoundFactory;
import engine.factory.TextureFactory;
import engine.factory.TexturedModelFactory;
import engine.loader.Loader;
import engine.renderer.MasterRenderer;
import engine.space.AABB;
import engine.space.QuadTree;
import engine.textures.GuiTexture;
import engine.textures.TerrainTexturePack;
import engine.toolbox.Colour;
import engine.toolbox.Maths;
import engine.toolbox.MousePicker;
import game.entities.EntityFern;
import game.entities.EntityPlayer;
import game.entities.EntityRocks;
import game.entities.EntityTreePine;

public class Main {
	private static MasterRenderer renderer;

	private static QuadTree<Entity> quadtree;

	private static List<Entity> normalMapEntities;
	private static List<Light> lights;
	private static List<Terrain> terrains;
	private static List<WaterTile> waters;
	private static List<GuiTexture> guis;

	private static Camera camera;
	private static Entity player;
	private static MousePicker picker;

	public static void main(String[] args) throws IOException {
		initalize();
		createWorld();
		gameLoop();
		cleanUp();
	}

	private static void initalize() {
		new DeviceDisplay();
		new DeviceAudio();
		new Loader();

		new SoundFactory("audio/");
		new RawModelFactory("");
		new TextureFactory("");
		new TexturedModelFactory();

		quadtree = new QuadTree<Entity>();
		renderer = new MasterRenderer();
		normalMapEntities = new ArrayList<Entity>();
		lights = new ArrayList<Light>();
		terrains = new ArrayList<Terrain>();
		waters = new ArrayList<WaterTile>();
		guis = new ArrayList<GuiTexture>();
		camera = new Camera();
	}

	private static void createWorld() throws IOException {
		// ***** CREATE TERRAINS
		TerrainTexturePack texturePack = new TerrainTexturePack(Loader.loadGameTexture("grassy2"), Loader.loadGameTexture("mud"), Loader.loadGameTexture("grassFlowers"), Loader.loadGameTexture("path"));
		Terrain terrain = new Terrain(0, -1, texturePack, Loader.loadGameTexture("blendMap"), "heightmap");
		terrains.add(terrain);

		// ***** CREATE NORMAL MAPPED ENTITIES
		// TexturedModel barrelModel = new TexturedModel(ObjLoaderNormalMapped.loadOBJNormalMapped("barrel"), new GameTexture(Loader.loadTexture("barrel"), Loader.loadTexture("barrelNormal"), 10.0f, 0.5f));
		// TexturedModel boulderModel = new TexturedModel(ObjLoaderNormalMapped.loadOBJNormalMapped("boulder"), new GameTexture(Loader.loadTexture("boulder"), Loader.loadTexture("boulderNormal"), 10.0f, 0.5f));
		// TexturedModel crateModel = new TexturedModel(ObjLoaderNormalMapped.loadOBJNormalMapped("crate"), new GameTexture(Loader.loadTexture("crate"), Loader.loadTexture("crateNormal"), 10.0f, 0.5f));

		// ***** PLACE ENTITIES INTO THE WORLD
		// Entity entity = new Entity(barrelModel, new Vector3f(75.0f, 10.0f, -75.0f), new Vector3f(0.0f, 0.0f, 0.0f), 1.0f);
		// Entity entity2 = new Entity(boulderModel, new Vector3f(85.0f, 10.0f, -75.0f), new Vector3f(0.0f, 0.0f, 0.0f), 1.0f);
		// Entity entity3 = new Entity(crateModel, new Vector3f(65.0f, 10.0f, -75.0f), new Vector3f(0.0f, 0.0f, 0.0f), 0.04f);
		// normalMapEntities.add(entity);
		// normalMapEntities.add(entity2);
		// normalMapEntities.add(entity3);

		Random random = new Random(5666778);

		for (int i = 0; i < 60; i++) {
			if (i % 3 == 0) {
				float x = random.nextFloat() * 150;
				float z = random.nextFloat() * -150;

				if (x > 50 && x < 100 || z < -50 && z > -100) {
				} else {
					float y = Maths.getTerrainHeight(x, z, terrains);
					new EntityFern(quadtree, new Vector3f(x, y, z), new Vector3f(0, random.nextFloat() * 360, 0));
				}
			}

			if (i % 2 == 0) {
				float x = random.nextFloat() * 150;
				float z = random.nextFloat() * -150;

				if (x > 50 && x < 100 || z < -50 && z > -100) {
				} else {
					float y = Maths.getTerrainHeight(x, z, terrains);
					new EntityTreePine(quadtree, new Vector3f(x, y, z), new Vector3f(0, random.nextFloat() * 360, 0), random.nextFloat() * 0.6f + 0.8f);
				}
			}
		}

		new EntityRocks(quadtree, new Vector3f(75, 4.6f, -75), new Vector3f(0, 0, 0), 75);

		// ***** OTHER SETUP
		lights.add(new Light(new Vector3f(10000, 10000, -10000), new Colour(1.3f, 1.3f, 1.3f)));

		player = new EntityPlayer(quadtree, new Vector3f(75, 5, -75), new Vector3f(0, 100, 0), "PLAYER");

		waters.add(new WaterTile(75, -75, 0));

		picker = new MousePicker(camera, renderer.getProjectionMatrix());
	}

	private static void gameLoop() {
		while (!Display.isCloseRequested()) {
			Set<Entity> entities = quadtree.queryRange(new HashSet<Entity>(), new AABB());

			for (Light l : lights) {
				renderer.processLight(l);
			}

			for (Entity e : entities) {
				e.update(DeviceDisplay.getCurrentTime());
				renderer.processEntity(e);
			}

			for (Entity e : normalMapEntities) {
				renderer.processNormalMapEntity(e);
			}

			for (Terrain t : terrains) {
				renderer.processTerrain(t);
			}

			for (WaterTile w : waters) {
				renderer.processWater(w);
			}

			for (GuiTexture g : guis) {
				renderer.processGUI(g);
			}

			((PlayerComponent) player.getComponent(PlayerComponent.ID)).move(terrains);
			camera.move((PlayerComponent) player.getComponent(PlayerComponent.ID), terrains, waters);
			picker.update(terrains);

			renderer.renderScene(camera);
			DeviceDisplay.updateDisplay();
		}
	}

	private static void cleanUp() {
		renderer.cleanUp();
		Loader.cleanUp();
		DeviceDisplay.cleanUp();
		DeviceAudio.cleanUp();
	}
}
