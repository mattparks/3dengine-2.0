package engine.entity;

public class WaterTile {
	public static final float TILE_SIZE = 60;

	private float height;
	private float x, z;

	public WaterTile(float centerX, float centerZ, float height) {
		this.height = height;
		x = centerX;
		z = centerZ;
	}

	public float getHeight() {
		return height;
	}

	public float getX() {
		return x;
	}

	public float getZ() {
		return z;
	}

	public float getHeightWorld(float worldX, float worldZ) {
		if (worldX > x || worldX < x || worldZ > z || worldX < z) {
			return 0;
		}

		return height;
	}
}
