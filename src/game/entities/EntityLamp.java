package game.entities;

import java.io.IOException;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.components.AABBComponent;
import engine.entity.components.ColliderComponent;
import engine.entity.components.CollisionComponent;
import engine.entity.components.LightComponent;
import engine.entity.components.ModelComponent;
import engine.factory.TexturedModelFactory;
import engine.models.TexturedModel;
import engine.space.ISpatialStructure;
import engine.toolbox.Attenuation;
import engine.toolbox.Colour;

public class EntityLamp extends Entity {
	public EntityLamp(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation) throws IOException {
		super(structure, position, rotation, 1.0f, null);
		new AABBComponent(this);
		new ColliderComponent(this);
		new CollisionComponent(this);
		TexturedModel texturedModel = TexturedModelFactory.get("lamp", "lamp");
		new ModelComponent(this, texturedModel, 0);
		new LightComponent(this, new Vector3f(0, 14, 0), new Attenuation(1, 0.01f, 0.002f), new Colour(1, 1, 1));
	}
}
