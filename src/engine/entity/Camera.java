package engine.entity;

import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

import engine.entity.components.PlayerComponent;
import engine.toolbox.Maths;

public class Camera {
	private float MIN_PLAYER_ZOOM = 10;
	private float MAX_PLAYER_ZOOM = 500;
	private float MIN_PLAYER_PITCH = -90;
	private float MAX_PLAYER_PITCH = 90;
	private float PLAYER_HEAD_HEIGHT = 10.0f;
	private float MOUSE_SENSITIVITY = 0.1f;

	private Vector3f position;
	private float pitch, yaw, roll;

	private float distanceFromPlayer, angleAroundPlayer;
	private float mouseAngleChange, mousePitchChange;

	private boolean grabbed;
	private boolean firstPerson;

	public Camera() {
		position = new Vector3f(0, 0, 0);
		pitch = 20;

		distanceFromPlayer = 25;
		angleAroundPlayer = 0;
		grabbed = false;
		firstPerson = false;
	}

	public void move(PlayerComponent player, List<Terrain> terrains, List<WaterTile> waters) {
		if (firstPerson) {
			firstPerson(player);
		} else {
			thirdPerson(player, terrains, waters);
		}
	}

	private void firstPerson(PlayerComponent player) {
		position = new Vector3f(player.getEntity().getPosition().getX(), player.getEntity().getPosition().getY() + PLAYER_HEAD_HEIGHT, player.getEntity().getPosition().getZ());
		yaw = 180 - player.getEntity().getRotation().getY();
		pitch = 0;

		// this.yaw += Mouse.getDX();
		// this.pitch += -Mouse.getDY();

		if (Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
			setGrabMouse(!grabbed);
		}
	}

	private void thirdPerson(PlayerComponent player, List<Terrain> terrains, List<WaterTile> waters) {
		calculateZoom();
		calculatePitch();
		calculateAngleAroundPlayer();

		float horizonalDistance = calculateHorizontalDistance();
		float verticalDistance = calculateVerticalDistance();

		float terrainHeight = Maths.getTerrainHeight(position.getX(), position.getZ(), terrains);
		float waterHeight = Maths.getWaterHeight(position.getX(), position.getZ(), waters);
		float minHeight = terrainHeight > waterHeight ? terrainHeight : waterHeight;

		if (waterHeight == 0 && terrainHeight == 0) {
			minHeight = Float.NEGATIVE_INFINITY;
		}

		calculateCameraPosition(horizonalDistance, verticalDistance, minHeight, player);
		yaw = 180 - (player.getEntity().getRotation().getY() + angleAroundPlayer);
	}

	private void calculateCameraPosition(float horizonalDistance, float verticalDistance, float minHeight, PlayerComponent player) {
		float theta = player.getEntity().getRotation().getY() + angleAroundPlayer;
		float offsetX = (float) (horizonalDistance * Math.sin(Math.toRadians(theta)));
		float offsetZ = (float) (horizonalDistance * Math.cos(Math.toRadians(theta)));
		position.x = player.getEntity().getPosition().x - offsetX;
		position.z = player.getEntity().getPosition().z - offsetZ;
		position.y = player.getEntity().getPosition().y + verticalDistance + PLAYER_HEAD_HEIGHT;

		if (position.y < minHeight) {
			position.y = minHeight + 1f;
		}
	}

	private void calculateZoom() {
		float zoomLevel = Mouse.getDWheel() * MOUSE_SENSITIVITY;

		if (!(distanceFromPlayer - zoomLevel <= MIN_PLAYER_ZOOM) && !(distanceFromPlayer - zoomLevel >= MAX_PLAYER_ZOOM)) {
			distanceFromPlayer -= zoomLevel;
		}
	}

	private void calculatePitch() {
		if (Mouse.isButtonDown(0)) {
			mousePitchChange = Mouse.getDY() * MOUSE_SENSITIVITY;
		} else {
			mousePitchChange += -mousePitchChange / 7;
		}

		pitch -= mousePitchChange;

		if (pitch <= MIN_PLAYER_PITCH) {
			pitch = MIN_PLAYER_PITCH;
		} else if (pitch >= MAX_PLAYER_PITCH) {
			pitch = MAX_PLAYER_PITCH;
		}
	}

	private void calculateAngleAroundPlayer() {
		if (Mouse.isButtonDown(0)) {
			mouseAngleChange = Mouse.getDX() * (MOUSE_SENSITIVITY * 2);
		} else {
			mouseAngleChange += -mouseAngleChange / 7;
		}

		angleAroundPlayer -= mouseAngleChange;
	}

	private float calculateHorizontalDistance() {
		return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
	}

	private float calculateVerticalDistance() {
		return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
	}

	public boolean isGrabbed() {
		return grabbed;
	}

	public void setGrabMouse(boolean grabbed) {
		this.grabbed = grabbed;
		Mouse.setGrabbed(grabbed);
	}

	public void switchViewType() {
		firstPerson = !firstPerson;
		setGrabMouse(firstPerson);
	}

	public void invertPitch() {
		pitch = -pitch;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}
}
