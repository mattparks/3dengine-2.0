package engine.entity.components;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.toolbox.IDAssigner;

/**
 * Performs some function when an entity is removed.
 */
public abstract class RemoveComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private boolean activated;

	/**
	 * Creates a new RemoveComponent.
	 *
	 * @param entity The entity this component is attached to.
	 */
	public RemoveComponent(Entity entity) {
		super(entity, ID);
		activated = false;
	}

	/**
	 * Called when the entity is first removed.
	 */
	public abstract void onActivate();

	/**
	 * Called every update after the entity is removed.
	 *
	 * @param delta How much time has passed since the last update.
	 */
	public abstract void removeUpdate(double delta);

	/**
	 * Activates this component. Calls the onActivate function, and begins calling the removeUpdate function on every update.
	 */
	public void activate() {
		activated = true;
		onActivate();
	}

	@Override
	public void update(double delta) {
		if (!activated) {
			return;
		}

		removeUpdate(delta);
	}
}
