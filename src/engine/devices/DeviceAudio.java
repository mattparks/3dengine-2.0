package engine.devices;

import static org.lwjgl.openal.AL10.AL_BUFFER;
import static org.lwjgl.openal.AL10.AL_FALSE;
import static org.lwjgl.openal.AL10.AL_FORMAT_MONO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_MONO8;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO16;
import static org.lwjgl.openal.AL10.AL_FORMAT_STEREO8;
import static org.lwjgl.openal.AL10.AL_GAIN;
import static org.lwjgl.openal.AL10.AL_LOOPING;
import static org.lwjgl.openal.AL10.AL_PAUSED;
import static org.lwjgl.openal.AL10.AL_PITCH;
import static org.lwjgl.openal.AL10.AL_PLAYING;
import static org.lwjgl.openal.AL10.AL_SOURCE_STATE;
import static org.lwjgl.openal.AL10.AL_TRUE;
import static org.lwjgl.openal.AL10.alBufferData;
import static org.lwjgl.openal.AL10.alDeleteBuffers;
import static org.lwjgl.openal.AL10.alDeleteSources;
import static org.lwjgl.openal.AL10.alGenBuffers;
import static org.lwjgl.openal.AL10.alGenSources;
import static org.lwjgl.openal.AL10.alGetSourcei;
import static org.lwjgl.openal.AL10.alSourcePause;
import static org.lwjgl.openal.AL10.alSourcePlay;
import static org.lwjgl.openal.AL10.alSourceStop;
import static org.lwjgl.openal.AL10.alSourcef;
import static org.lwjgl.openal.AL10.alSourcei;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;

import engine.sound.Sound;
import engine.sound.SoundData;

/**
 * An Audio Device implemented using OpenAL.
 */
public class DeviceAudio {
	public static class AudioChannel {
		public int source;

		public AudioChannel() {
			source = alGenSources();
		}

		public void dispose() {
			if (source != 0) {
				alDeleteSources(source);
			}

			source = 0;
		}

		public boolean isFree() {
			int state = alGetSourcei(source, AL_SOURCE_STATE);
			return state != AL_PLAYING && state != AL_PAUSED;
		}
	}

	public static class AudioObject {
		public int dataId;
		public float volume;
		public float pitch;
		public boolean loop;

		public AudioObject(int dataId, float volume, float pitch, boolean loop) {
			this.dataId = dataId;
			this.volume = volume;
			this.pitch = pitch;
			this.loop = loop;
		}
	}

	/** Format for 8 bit mono audio. */
	public static final int FORMAT_MONO_8 = AL_FORMAT_MONO8;
	/** Format for 16 bit mono audio. */
	public static final int FORMAT_MONO_16 = AL_FORMAT_MONO16;
	/** Format for 8 bit stereo audio. */
	public static final int FORMAT_STEREO_8 = AL_FORMAT_STEREO8;
	/** Format for 16 bit stereo audio. */
	public static final int FORMAT_STEREO_16 = AL_FORMAT_STEREO16;

	private static Map<Integer, AudioObject> objects;
	private static Map<Integer, AudioChannel> channels;
	private static int currentObjectId;

	/**
	 * Creates a new OpenALAudioDevice.
	 *
	 * @throws LWJGLException If the audio device could not be created.
	 */
	public DeviceAudio() {
		try {
			AL.create();
		} catch (LWJGLException e) {
			System.err.println("Could not create the audio device!");
			e.printStackTrace();
		}

		channels = new HashMap<>();
		objects = new HashMap<>();
		currentObjectId = 1;
	}

	/**
	 * Creates a piece of audio data that can be used by this audio device.
	 * <p>
	 * It is preferred to use this through the {@link SoundData} class when possible.
	 *
	 * @param data The raw array of bytes specifying the piece of audio.
	 * @param format The format of {@code data}. Should be one of the IAudioDevice.FORMAT options.
	 * @param sampleRate The number of samples {@code data} specifies per second of audio.
	 * @param isBigEndian True if {@code data} is in big endian byte ordering; false otherwise.
	 * @return An integer identifying this audio data on this device.
	 */
	public static int createAudioData(byte[] data, int format, int sampleRate, boolean isBigEndian) {
		int buffer = alGenBuffers();
		boolean isStereo = format == FORMAT_STEREO_16 || format == FORMAT_MONO_16;
		alBufferData(buffer, format, toByteBuffer(data, isStereo, isBigEndian), sampleRate);
		return buffer;
	}

	private static ByteBuffer toByteBuffer(byte[] data, boolean isStereo, boolean isBigEndian) {
		ByteBuffer destination = ByteBuffer.allocateDirect(data.length);
		destination.order(ByteOrder.nativeOrder());
		ByteBuffer source = ByteBuffer.wrap(data);
		source.order(isBigEndian ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);

		if (isStereo) {
			ShortBuffer destAsShort = destination.asShortBuffer();
			ShortBuffer srcAsShort = source.asShortBuffer();

			while (srcAsShort.hasRemaining()) {
				destAsShort.put(srcAsShort.get());
			}
		} else {
			while (source.hasRemaining()) {
				destination.put(source.get());
			}
		}

		destination.rewind();
		return destination;
	}

	/**
	 * Releases and invalidates a piece of audio. The value of {@code dataId} will be invalid after this call and may be reused to identify a new piece of audio. Note that this does nothing when the null id, 0, is given for {@code dataId}.
	 * <p>
	 * It is preferred to use this through the {@link SoundData} class when possible.
	 *
	 * @param dataId The integer identifying the audio data to be released.
	 * @return The null dataId of 0.
	 */
	public static int releaseAudioData(int dataId) {
		if (dataId != 0) {
			alDeleteBuffers(dataId);
		}

		return 0;
	}

	/**
	 * Creates an audio object that completely specifies a playable piece of sound.
	 * <p>
	 * It is preferred to use this through the {@link Sound} class when possible.
	 *
	 * @param dataId The integer that identifies the audio data being played.
	 * @param volume How loud the audio should be played at. 1.0 specifies normal volume, and lower or higher values specify quieter or louder volumes, respectively.
	 *            <p>
	 *            If the device is incapable of playing audio at the specified volume, then the audio should be played at the closest possible volume to the volume specified.
	 * @param pitch What pitch the audio should be played at. 1.0 specifies normal pitch, and lower or higher values specify higher or lower pitches, respectively.
	 *            <p>
	 *            If the device is incapable of playing audio at the specified pitch, then the audio should be played at the closest possible pitch to the pitch specified.
	 * @param shouldLoop Whether the audio should automatically restart when finished playing.
	 * @return An integer identifying this audio object on the device.
	 */
	public static int createAudioObject(int dataId, float volume, float pitch, boolean shouldLoop) {
		AudioObject object = new AudioObject(dataId, volume, pitch, shouldLoop);
		int id = currentObjectId++;
		objects.put(id, object);
		return id;
	}

	/**
	 * Updates an existing audio object with new parameters.
	 * <p>
	 * It is preferred to use this through the {@link Sound} class when possible.
	 *
	 * @param objectId The integer that identifies the audio object being updated.
	 * @param volume How loud the audio should be played at. 1.0 specifies normal volume, and lower or higher values specify quieter or louder volumes, respectively.
	 *            <p>
	 *            If the device is incapable of playing audio at the specified volume, then the audio should be played at the closest possible volume to the volume specified.
	 * @param pitch What pitch the audio should be played at. 1.0 specifies normal pitch, and lower or higher values specify higher or lower pitches, respectively.
	 *            <p>
	 *            If the device is incapable of playing audio at the specified pitch, then the audio should be played at the closest possible pitch to the pitch specified.
	 * @param shouldLoop Whether the audio should automatically restart when finished playing.
	 */
	public static void updateAudioObject(int objectId, float volume, float pitch, boolean shouldLoop) {
		if (objectId == 0) {
			return;
		}

		AudioObject object = objects.get(objectId);
		object.volume = volume;
		object.pitch = pitch;
		object.loop = shouldLoop;
	}

	/**
	 * Releases and invalidates an audio object. The value of {@code objectId} will be invalid after this call and may be reused to identify a new audio object. Note that this does nothing when the null id, 0, is given for {@code objectId}.
	 * <p>
	 * It is preferred to use this through the {@link Sound} class when possible.
	 *
	 * @param objectId The integer identifying the audio object to be released.
	 * @return The null objectId of 0.
	 */
	public static int releaseAudioObject(int objectId) {
		if (objectId != 0) {
			objects.remove(objectId);
		}

		return 0;
	}

	/**
	 * Plays an audio object through this audio device. If it is already playing, it is stopped and restarted. When the audio is finished playing, it will stop if the object's shouldLoop is false, or it will restart if the object's shouldLoop is true.
	 * <p>
	 * It is preferred to use this through the {@link Sound} class when possible.
	 *
	 * @param objectId The integer identifying the audio object to be played.
	 */
	public static void play(int objectId) {
		AudioChannel channel = getAudioChannel(objectId);
		AudioObject object = objects.get(objectId);
		int source = channel.source;
		alSourcei(source, AL_BUFFER, object.dataId);
		alSourcef(source, AL_PITCH, object.pitch);
		alSourcef(source, AL_GAIN, object.volume);
		alSourcei(source, AL_LOOPING, object.loop ? AL_TRUE : AL_FALSE);
		alSourcePlay(source);
	}

	private static AudioChannel getAudioChannel(int objectId) {
		if (channels.containsKey(objectId)) {
			return channels.get(objectId);
		}

		Iterator<Entry<Integer, AudioChannel>> it = channels.entrySet().iterator();

		while (it.hasNext()) {
			Entry<Integer, AudioChannel> entry = it.next();

			if (entry.getValue().isFree()) {
				AudioChannel result = entry.getValue();
				it.remove();
				channels.put(objectId, result);
				return result;
			}
		}

		AudioChannel result = new AudioChannel();
		channels.put(objectId, result);
		return result;
	}

	/**
	 * Stops playing an audio object through this device, but leaves it's position alone. When the object is played again, it should start where it left off.
	 * <p>
	 * It is preferred to use this through the {@link Sound} class when possible.
	 *
	 * @param objectId The integer identifying the audio object to be paused.
	 */
	public static void pause(int objectId) {
		AudioChannel channel = channels.get(objectId);

		if (channel != null) {
			alSourcePause(channel.source);
		}
	}

	/**
	 * Stops playing an audio object through this device and resets its position to the start of the audio. When the object is played again, it should start at the beginning.
	 * <p>
	 * It is preferred to use this through the {@link Sound} class when possible.
	 *
	 * @param objectId The integer identifying the audio object to be stopped.
	 */
	public static void stop(int objectId) {
		AudioChannel channel = channels.get(objectId);

		if (channel != null) {
			alSourceStop(channel.source);
		}
	}

	/**
	 * Releases all resources being used. This audio device should not be used after this is called.
	 */
	public static void cleanUp() {
		Iterator<Entry<Integer, AudioChannel>> it = channels.entrySet().iterator();

		while (it.hasNext()) {
			it.next().getValue().dispose();
		}

		AL.destroy();
	}
}
