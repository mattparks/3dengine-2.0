package engine.sound;

import engine.devices.DeviceAudio;

/**
 * Represents a playable sound.
 */
public class Sound {
	private int soundId;
	private float defaultVolume;
	private float volume;
	private float pitch;
	private boolean shouldLoop;

	/**
	 * Creates a new Sound. It is preferred to use this object instead of manipulating an {@link DeviceAudio}'s audio object methods directly.
	 *
	 * @param data The SoundData for the sound being played.
	 * @param volume How loud the sound should be. 1.0 specifies normal volume, and lower or higher values specify quieter or louder volumes, respectively.
	 * @param pitch What pitch the audio should be played at. 1.0 specifies normal pitch, and lower or higher values specify higher or lower pitches, respectively.
	 * @param shouldLoop Whether the audio should automatically restart when finished playing.
	 */
	public Sound(SoundData data, float volume, float pitch, boolean shouldLoop) {
		soundId = DeviceAudio.createAudioObject(data.getId(), volume, pitch, shouldLoop);
		defaultVolume = volume;
		this.volume = volume;
		this.pitch = pitch;
		this.shouldLoop = shouldLoop;
	}

	@Override
	protected void finalize() throws Throwable {
		dispose();
		super.finalize();
	}

	/**
	 * Plays this sound. If it is already playing, it is stopped and restarted. When the audio is finished playing, it will stop if the object's shouldLoop is false, or it will restart if the object's shouldLoop is true.
	 */
	public void play() {
		DeviceAudio.play(soundId);
	}

	/**
	 * Stops playing this sound, but leaves its position alone. When the object is played again, it should start where it left off.
	 */
	public void pause() {
		DeviceAudio.pause(soundId);
	}

	/**
	 * Stops playing this sound and resets its position to the start of the audio. When the object is played again, it should start at the beginning.
	 */
	public void stop() {
		DeviceAudio.stop(soundId);
	}

	/**
	 * Releases this object. This should be called when the object will no longer be used, and no methods or fields should be used after this method is called.
	 */
	public void dispose() {
		soundId = DeviceAudio.releaseAudioObject(soundId);
	}

	/**
	 * Gets the volume this object was initially created with.
	 *
	 * @return The volume this object was initially created with.
	 */
	public float getDefaultVolume() {
		return defaultVolume;
	}

	/**
	 * Sets the volume of this sound to a new value.
	 *
	 * @param newVolume The new volume to be set to. 1.0 specifies normal volume, and lower or higher values specify quieter or louder volumes, respectively.
	 */
	public void setVolume(float newVolume) {
		volume = newVolume;
		DeviceAudio.updateAudioObject(soundId, volume, pitch, shouldLoop);
	}
}
