package engine.space;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Represents a 3D space.
 */
public class QuadTree<T extends ISpatialObject> implements ISpatialStructure<T> {
	private List<T> objects;

	/**
	 * Initializes a QuadTree from an AABB.
	 */
	public QuadTree() {
		objects = new ArrayList<>();
	}

	@Override
	public void add(T object) {
		objects.add(object);
	}

	@Override
	public void remove(T object) {
		objects.remove(object);
	}

	@Override
	public void clear() {
		objects.clear();
	}

	@Override
	public Set<T> getAll(Set<T> result) {
		return addAll(result);
	}

	private Set<T> addAll(Set<T> result) {
		result.addAll(objects);
		return result;
	}

	@Override
	public Set<T> queryRange(Set<T> result, AABB range) {
		Iterator<T> it = objects.iterator();

		while (it.hasNext()) {
			T current = it.next();

			// if (current.getAABB().intersects(range)) {
			result.add(current);
			// }
		}

		return result;
	}

	public int getSize() {
		return objects.size();
	}
}
