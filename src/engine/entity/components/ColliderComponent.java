package engine.entity.components;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.space.AABB;
import engine.toolbox.IDAssigner;

/**
 * Gives an object a collider for spatial interaction. Note that a collider doesn't necessarily need to be used for collision. A collider component can be used for any spatial interaction.
 * <p>
 * For example, a checkpoint can use a ColliderComponent to detect when the player has reached it.
 */
public class ColliderComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private AABB aabb;

	/**
	 * Creates a new ColliderComponent.
	 *
	 * @param entity The entity this component is attached to.
	 */
	public ColliderComponent(Entity entity) {
		super(entity, ID);
		aabb = null;
	}

	/**
	 * Ensures the bounds of the collider are at least big enough to contain {@code other}.
	 *
	 * @param other The AABB this collider must be able to contain.
	 */
	public void fitAABB(AABB other) {
		if (aabb == null) {
			aabb = other;
		} else {
			aabb = aabb.combine(other);
		}

		((AABBComponent) getEntity().getComponent(AABBComponent.ID)).fitAABB(aabb);
	}

	/**
	 * Gets the AABB representing the collision range.
	 *
	 * @return An AABB representing the collision range.
	 */
	public AABB getAABB() {
		return ((AABBComponent) getEntity().getComponent(AABBComponent.ID)).translateAABB(aabb);
	}

	@Override
	public void update(double delta) {
	}
}
