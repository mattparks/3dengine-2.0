package engine.models;

import engine.space.AABB;

public class RawModel {
	private int vaoID;
	private int vertexCount;
	private final AABB aabb;

	public RawModel(int vaoID, int vertexCount, AABB aabb) {
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
		this.aabb = aabb;
	}

	public int getVaoID() {
		return vaoID;
	}

	public int getVertexCount() {
		return vertexCount;
	}

	public AABB getAABB() {
		return aabb;
	}
}
