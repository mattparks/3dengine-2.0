package engine.factory;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import engine.loader.obj.OBJLoader;
import engine.models.RawModel;

/**
 * A factory for creating raw models.
 */
public class RawModelFactory {
	private static String filePath;
	private static Map<String, SoftReference<RawModel>> loaded;

	/**
	 * Creates a new Raw Model Factory.
	 *
	 * @param filePath The base path for sound files.
	 */
	public RawModelFactory(String filePath) {
		RawModelFactory.filePath = filePath;
		RawModelFactory.loaded = new HashMap<>();
	}

	/**
	 * Gets a new object from the factory. If the desired raw model data is already loaded, then the raw model data is reused and not loaded again. Otherwise, the raw model data is loaded from the specified file.
	 *
	 * @param fileName The file path for model file.
	 * @return Returns the newly created raw model object.
	 */
	public static RawModel get(String fileName) {
		fileName = filePath + fileName;
		SoftReference<RawModel> ref = loaded.get(fileName);
		RawModel data = ref == null ? null : ref.get();

		if (data == null) {
			loaded.remove(fileName);
			data = OBJLoader.loadObjModel(fileName);
			loaded.put(fileName, new SoftReference<RawModel>(data));
		}

		return data;
	}

	public static String getFilePath() {
		return filePath;
	}
}
