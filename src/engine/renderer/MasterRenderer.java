package engine.renderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import engine.devices.DeviceDisplay;
import engine.entity.Camera;
import engine.entity.Entity;
import engine.entity.Light;
import engine.entity.Terrain;
import engine.entity.WaterTile;
import engine.entity.components.ModelComponent;
import engine.models.TexturedModel;
import engine.textures.GuiTexture;
import engine.toolbox.Maths;

public class MasterRenderer {
	private static final float FOV = 70;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 1000;

	public static final float SKY_RED = 0.1f;
	public static final float SKY_GREEN = 0.4f;
	public static final float SKY_BLUE = 0.2f;

	private Matrix4f projectionMatrix;
	private FrameBuffers buffers;

	private RendererEntity entityRenderer;
	private RendererNormalMapped normalMapRenderer;
	private RendererTerrain terrainRenderer;
	private RendererSkybox skyboxRenderer;
	private RendererWater waterRenderer;
	private RendererGui guiRenderer;

	private List<Light> lights = new ArrayList<Light>();
	private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
	private Map<TexturedModel, List<Entity>> normalMapEntities = new HashMap<TexturedModel, List<Entity>>();
	private List<Terrain> terrains = new ArrayList<Terrain>();
	private List<WaterTile> waters = new ArrayList<WaterTile>();
	private List<GuiTexture> guis = new ArrayList<GuiTexture>();

	public MasterRenderer() {
		enableCulling();
		buffers = new FrameBuffers();
		projectionMatrix = Maths.createPerspectiveProjectionMatrix(FOV, DeviceDisplay.getDisplayRatio(), NEAR_PLANE, FAR_PLANE);
		entityRenderer = new RendererEntity(projectionMatrix);
		normalMapRenderer = new RendererNormalMapped(projectionMatrix);
		terrainRenderer = new RendererTerrain(projectionMatrix);
		skyboxRenderer = new RendererSkybox(projectionMatrix);
		waterRenderer = new RendererWater(projectionMatrix, buffers);
		guiRenderer = new RendererGui();
	}

	public Matrix4f getProjectionMatrix() {
		return projectionMatrix;
	}

	public void renderScene(Camera camera) {
		GL11.glEnable(GL30.GL_CLIP_DISTANCE0);

		// Render reflection teture
		buffers.bindReflectionFrameBuffer();
		float distance = 2 * (camera.getPosition().y - waters.get(0).getHeight());
		camera.getPosition().y -= distance;
		camera.invertPitch();
		render(camera, new Vector4f(0, 1, 0, -waters.get(0).getHeight() + 1));
		camera.getPosition().y += distance;
		camera.invertPitch();

		// Render refraction texture
		buffers.bindRefractionFrameBuffer();
		render(camera, new Vector4f(0, -1, 0, waters.get(0).getHeight()));

		// Render to screen
		GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
		buffers.unbindCurrentFrameBuffer();
		render(camera, new Vector4f(0, -1, 0, Float.POSITIVE_INFINITY));
		waterRenderer.render(waters, camera, lights.get(0));
		guiRenderer.render(guis);

		lights.clear();
		entities.clear();
		normalMapEntities.clear();
		terrains.clear();
		waters.clear();
		guis.clear();
	}

	public void render(Camera camera, Vector4f clipPlane) {
		prepare();
		entityRenderer.render(entities, lights, camera, clipPlane);
		normalMapRenderer.render(normalMapEntities, clipPlane, lights, camera);
		terrainRenderer.render(terrains, lights, camera, clipPlane);
		skyboxRenderer.render(camera, SKY_RED, SKY_GREEN, SKY_BLUE);
	}

	public void prepare() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(SKY_RED, SKY_GREEN, SKY_BLUE, 1);
	}

	public static void enableCulling() {
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}

	public static void disableCulling() {
		GL11.glDisable(GL11.GL_CULL_FACE);
	}

	public void processLight(Light light) {
		lights.add(light);
	}

	public void processEntity(Entity entity) {
		ModelComponent mc = (ModelComponent) entity.getComponent(ModelComponent.ID);

		if (mc != null) {
			TexturedModel entityModel = mc.getModel();
			List<Entity> batch = entities.get(entityModel);

			if (batch != null) {
				batch.add(entity);
			} else {
				List<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				entities.put(entityModel, newBatch);
			}
		}
	}

	public void processNormalMapEntity(Entity entity) {
		ModelComponent mc = (ModelComponent) entity.getComponent(ModelComponent.ID);

		if (mc != null) {
			TexturedModel entityModel = mc.getModel();
			List<Entity> batch = normalMapEntities.get(entityModel);

			if (batch != null) {
				batch.add(entity);
			} else {
				List<Entity> newBatch = new ArrayList<Entity>();
				newBatch.add(entity);
				normalMapEntities.put(entityModel, newBatch);
			}
		}
	}

	public void processTerrain(Terrain terrain) {
		terrains.add(terrain);
	}

	public void processWater(WaterTile water) {
		waters.add(water);
	}

	public void processGUI(GuiTexture gui) {
		guis.add(gui);
	}

	public void cleanUp() {
		buffers.cleanUp();
		entityRenderer.cleanUp();
		normalMapRenderer.cleanUp();
		terrainRenderer.cleanUp();
		waterRenderer.cleanUp();
		guiRenderer.cleanUp();
	}
}
