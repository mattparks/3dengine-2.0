package engine.toolbox;

import org.lwjgl.util.vector.Vector3f;

public class Colour {
	private float red;
	private float green;
	private float blue;

	/**
	 * Creates a new Colour.
	 *
	 * @param red The amount of red, in the range of (0, 1).
	 * @param green The amount of green, in the range of (0, 1).
	 * @param blue The amount of blue, in the range of (0, 1).
	 */
	public Colour(float red, float green, float blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public Vector3f toVector() {
		return new Vector3f(red, green, blue);
	}

	public float getRed() {
		return red;
	}

	public void setRed(float red) {
		this.red = red;
	}

	public float getGreen() {
		return green;
	}

	public void setGreen(float green) {
		this.green = green;
	}

	public float getBlue() {
		return blue;
	}

	public void setBlue(float blue) {
		this.blue = blue;
	}
}
