package engine.entity;

import org.lwjgl.util.vector.Vector3f;

import engine.toolbox.Attenuation;
import engine.toolbox.Colour;

public class Light {
	private Vector3f position;
	private Colour colour;
	private Attenuation attenuation;

	public Light(Vector3f position, Colour colour) {
		this(position, colour, new Attenuation(1, 0, 0));
	}

	public Light(Vector3f position, Colour colour, Attenuation attenuation) {
		this.position = position;
		this.colour = colour;
		this.attenuation = attenuation;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public Attenuation getAttenuation() {
		return attenuation;
	}

	public void setAttenuation(Attenuation attenuation) {
		this.attenuation = attenuation;
	}
}
