package engine.entity.components;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.entity.IEntityVisitor;
import engine.space.AABB;
import engine.toolbox.IDAssigner;

/**
 * Component that detects collision between two entities.
 * <p>
 * Note: this component requires that both entities have a ColliderComponent. Should one entity not have a ColliderComponent, then no collisions will be detected, because there is no collider to detect collisions against.
 */
public class CollisionComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();

	/**
	 * Creates a new CollisionComponent.
	 *
	 * @param entity The entity this component is attached to.
	 */
	public CollisionComponent(Entity entity) {
		super(entity, ID);
	}

	/**
	 * Resolves AABB collisions with any other CollisionComponents encountered.
	 *
	 * @param amount The amount attempting to be moved.
	 * @return New move vector that will not cause collisions after movement.
	 */
	public Vector3f resolveAABBCollisions(Vector3f amount) {
		Vector3f result = new Vector3f(amount.getX(), amount.getY(), amount.getZ());
		ColliderComponent c = (ColliderComponent) getEntity().getComponent(ColliderComponent.ID);

		if (c == null) {
			return result;
		}

		final AABB collider = c.getAABB();
		final AABB collisionRange = collider.stretch(amount);

		getEntity().visitInRange(CollisionComponent.ID, collisionRange, new IEntityVisitor() {
			@Override
			public void visit(Entity entity, IEntityComponent component) {
				if (entity == getEntity()) {
					return;
				}

				ColliderComponent c2 = (ColliderComponent) entity.getComponent(ColliderComponent.ID);

				if (c2 == null) {
					return;
				}

				AABB collider2 = c2.getAABB();

				if (collider2.intersects(collisionRange)) {
					result.set((float) collider.resolveCollisionX(collider2, result.getX()), (float) collider.resolveCollisionY(collider2, result.getY()), (float) collider.resolveCollisionZ(collider2, result.getZ()));
				}
			}
		});

		return result;
	}

	@Override
	public void update(double delta) {
	}
}
