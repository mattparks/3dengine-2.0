package engine.entity.components;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.toolbox.IDAssigner;

/**
 * Links two objects such that when one is removed, the other is also removed.
 */
public class LinkedComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private Entity linked;

	/**
	 * Creates a new LinkComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param linked The entity which, if removed, also removes this component.
	 */
	public LinkedComponent(Entity entity, Entity linked) {
		super(entity, ID);
		this.linked = linked;
	}

	@Override
	public void update(double delta) {
		if (linked.isRemoved()) {
			getEntity().remove();
		}
	}
}
