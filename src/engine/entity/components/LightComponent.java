package engine.entity.components;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.toolbox.Attenuation;
import engine.toolbox.Colour;
import engine.toolbox.IDAssigner;

/**
 * Creates a light that can be rendered into the world.
 */
public class LightComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private Vector3f offset;
	private Attenuation attenuation;
	private Colour colour;

	public LightComponent(Entity entity, Vector3f offset, Colour colour) {
		this(entity, offset, new Attenuation(1, 0, 0), colour);
	}

	/**
	 * Creates a new LightComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param offset How much the light position should be offset from the entities position.
	 * @param attenuation How much the intencity of the light is lost over a distance.
	 * @param colour The colour of the light.
	 */
	public LightComponent(Entity entity, Vector3f offset, Attenuation attenuation, Colour colour) {
		super(entity, ID);
		this.attenuation = attenuation;
		this.colour = colour;
		this.offset = offset;
	}

	/**
	 * @return Gets the lights position relative too the world.
	 */
	public Vector3f getPosition() {
		Vector3f worldPos = new Vector3f();
		Vector3f.add(new Vector3f(super.getEntity().getPosition()), offset, worldPos);
		return worldPos;
	}

	public Vector3f getOffset() {
		return offset;
	}

	public void setOffset(Vector3f offset) {
		this.offset = offset;
	}

	public Attenuation getAttenuation() {
		return attenuation;
	}

	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	@Override
	public void update(double delta) {
	}
}
