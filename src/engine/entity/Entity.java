package engine.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.components.AudioComponent;
import engine.entity.components.CollisionComponent;
import engine.entity.components.RemoveComponent;
import engine.space.AABB;
import engine.space.ISpatialObject;
import engine.space.ISpatialStructure;

/**
 * A generic object in the game.
 */
public class Entity implements ISpatialObject, Comparable<Entity> {
	private static int currentId = 0;

	private ISpatialStructure<Entity> structure;
	private List<IEntityComponent> components;
	private Vector3f position;
	private Vector3f rotation;
	private float scale;
	private String name;
	private int id;
	private boolean isRemoved;

	private static int getNextId() {
		return currentId++;
	}

	/**
	 * Creates a new Entity with minimum necessary construction.
	 *
	 * @param structure The spatial structure this entity will be contained in.
	 * @param position The location of the entity.
	 * @param rotation The rotation of the entity.
	 * @param scale The scale of the entity.
	 * @param name The game for this entity. Can be null.
	 */
	public Entity(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale, String name) {
		this.structure = structure;
		components = new ArrayList<>();
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		this.name = name;
		id = getNextId();
		isRemoved = false;
		this.structure.add(this);
	}

	/**
	 * Finds and returns a component attached to this entity by id. If more than one is found, the first component in the list is returned. If none are found, returns null.
	 *
	 * @param id The id of the component. This is typically found with ComponentClass.ID.
	 * @return The first component found with the given id, or null if none are found.
	 */
	public IEntityComponent getComponent(int id) {
		Iterator<IEntityComponent> it = components.iterator();

		while (it.hasNext()) {
			IEntityComponent current = it.next();

			if (current.getId() == id) {
				return current;
			}
		}

		return null;
	}

	/**
	 * Adds a new component to the entity.
	 *
	 * @param component The component to add.
	 */
	public void addComponent(IEntityComponent component) {
		components.add(component);
	}

	/**
	 * Removes a component to the entity.
	 *
	 * @param component The component to remove.
	 */
	public void removeComponent(IEntityComponent component) {
		components.remove(component);
	}

	/**
	 * Removes a component from this entity by id. If more than one is found, the first component in the list is removed. If none are found, nothing is removed.
	 *
	 * @param id The id of the component. This is typically found with ComponentClass.ID.
	 */
	public void removeComponent(int id) {
		for (IEntityComponent c : components) {
			if (c.getId() == id) {
				components.remove(c);
			}
		}
	}

	/**
	 * Visits every entity with a particular component within a certain range of space.
	 *
	 * @param id The id of the component. This is typically found with ComponentClass.ID. If no particular component is desired, specify -1.
	 * @param range The range of space to be visited.
	 * @param visitor The visitor that will be executed for every entity visited.
	 */
	public void visitInRange(int id, AABB range, IEntityVisitor visitor) {
		Set<Entity> entities = structure.queryRange(new HashSet<Entity>(), range);
		Iterator<Entity> it = entities.iterator();

		while (it.hasNext()) {
			Entity entity = it.next();

			if (entity.isRemoved) {
				continue;
			}

			IEntityComponent component = id == -1 ? null : entity.getComponent(id);

			if (component != null || id == -1) {
				visitor.visit(entity, component);
			}
		}
	}

	/**
	 * Updates all the components attached to this entity.
	 *
	 * @param delta How much time has passed since the last update.
	 */
	public void update(double delta) {
		Iterator<IEntityComponent> it = components.iterator();

		while (it.hasNext()) {
			it.next().update(delta);
		}
	}

	/**
	 * Moves this entity by a certain amount. If this entity is a colliding entity and it hits another colliding entity when it moves, then this will only move the entity as far as it can without intersecting a colliding entity. This function only works on one axis at a time; one of the parameters must be 0.
	 *
	 * @param move The amount to move.
	 * @param rotate The amount to rotate.
	 */
	public void move(Vector3f move, Vector3f rotate) {
		structure.remove(this);
		float moveAmountX = move.getX();
		float moveAmountY = move.getY();
		float moveAmountZ = move.getZ();

		float rotateAmountX = rotate.getX();
		float rotateAmountY = rotate.getY();
		float rotateAmountZ = rotate.getZ();

		CollisionComponent collision = (CollisionComponent) getComponent(CollisionComponent.ID);

		if (collision != null) {
			Vector3f amts = collision.resolveAABBCollisions(new Vector3f(moveAmountX, moveAmountY, moveAmountZ));
			moveAmountX = amts.getX();
			moveAmountY = amts.getY();
			moveAmountZ = amts.getZ();
		}

		position.set(position.getX() + moveAmountX, position.getY() + moveAmountY, position.getZ() + moveAmountZ);
		rotation.set(rotation.getX() + rotateAmountX, rotation.getY() + rotateAmountY, rotation.getZ() + rotateAmountZ);
		structure.add(this);
	}

	/**
	 * Removes this entity from the spatial structure, and triggers any remove actions specified for this entity.
	 */
	public void remove() {
		if (isRemoved) {
			return;
		}

		AudioComponent audioComponent = (AudioComponent) getComponent(AudioComponent.ID);

		if (audioComponent != null) {
			audioComponent.play("remove");
		}

		isRemoved = true;
		RemoveComponent removeComponent = (RemoveComponent) getComponent(RemoveComponent.ID);

		if (removeComponent != null) {
			removeComponent.activate();
		} else {
			forceRemove();
		}
	}

	/**
	 * Gets whether or not this entity has been removed from the spatial structure.
	 *
	 * @return Whether or not this entity has been removed from the spatial structure.
	 */
	public boolean isRemoved() {
		return isRemoved;
	}

	/**
	 * Forcibly removes this entity from the spatial structure without triggering remove actions. Use with caution; this function may fail or cause errors if used inappropriately.
	 */
	public void forceRemove() {
		isRemoved = true;
		structure.remove(this);
	}

	/**
	 * Removes this entity just from the structure.
	 */
	public void removeFromStructure() {
		structure.remove(this);
	}

	/**
	 * Adds the entity into the component.
	 */
	public void addToStructure() {
		structure.add(this);
	}

	/**
	 * Gets the location of this entity.
	 *
	 * @return The location of this entity.
	 */
	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	/**
	 * Gets the rotation of this entity.
	 *
	 * @return The rotation of this entity.
	 */
	public Vector3f getRotation() {
		return rotation;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public String getName() {
		return name;
	}

	@Override
	public int compareTo(Entity other) {
		if (id > other.id) {
			return 1;
		}

		if (id < other.id) {
			return -1;
		}

		return 0;
	}
}
