package engine.entity.components;

import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import engine.devices.DeviceDisplay;
import engine.entity.Entity;
import engine.entity.IEntityComponent;
import engine.entity.Terrain;
import engine.toolbox.IDAssigner;
import engine.toolbox.Maths;

/**
 * Creates a new controllable player component for the game.
 */
public class PlayerComponent extends IEntityComponent {
	public static final int ID = IDAssigner.getId();
	private static final float RUN_SPEED = 40;
	private static final float TURN_SPEED = 160;
	private static final float JUMP_POWER = 30;
	private static final float GRAVITY = -50;

	private boolean isInAir;
	private float currentSpeed;
	private float currentTurnSpeed;
	private float currentUpwardsSpeed;
	private String username;

	/**
	 * Creates a new PlayerComponent.
	 *
	 * @param entity The entity this component is attached to.
	 * @param username The username represented by this player component.
	 */
	public PlayerComponent(Entity entity, String username) {
		super(entity, ID);
		isInAir = false;
		currentSpeed = 0;
		currentTurnSpeed = 0;
		currentUpwardsSpeed = 0;
		this.username = username;
	}

	/**
	 * Called when this entity is wanted to be controlled by this client.
	 *
	 * @param terrains The list of terrains the player can walk on.
	 * @return Returns weather or not the player has moved during this move update.
	 */
	public boolean move(List<Terrain> terrains) {
		checkInputs();

		Vector3f startPos = new Vector3f(super.getEntity().getPosition());
		Vector3f startRot = new Vector3f(super.getEntity().getRotation());

		float distance = currentSpeed * DeviceDisplay.getFrameTimeSeconds();
		float dx = (float) (distance * Math.sin(Math.toRadians(super.getEntity().getRotation().getY())));
		float dz = (float) (distance * Math.cos(Math.toRadians(super.getEntity().getRotation().getY())));
		float dy = currentUpwardsSpeed * DeviceDisplay.getFrameTimeSeconds();
		float ry = currentTurnSpeed * DeviceDisplay.getFrameTimeSeconds();

		float terrainHeight = Maths.getTerrainHeight(super.getEntity().getPosition().getX() + dx, super.getEntity().getPosition().getZ() + dz, terrains);
		currentUpwardsSpeed += GRAVITY * DeviceDisplay.getFrameTimeSeconds();

		if (super.getEntity().getPosition().getY() + dy < terrainHeight) {
			dy = terrainHeight - super.getEntity().getPosition().getY(); // Move back too the surface.
			isInAir = false;
			currentUpwardsSpeed = 0;
		}

		super.getEntity().move(new Vector3f(dx, dy, dz), new Vector3f(0, ry, 0));

		if (startPos.getX() == dx && startPos.getY() == dy && startPos.getZ() == dz) {
			if (startRot.getX() == super.getEntity().getRotation().getX() && startRot.getY() == super.getEntity().getRotation().getY() && startRot.getZ() == super.getEntity().getRotation().getZ()) {
				return false;
			}
		}

		return true;
	}

	private void jump() {
		if (!isInAir) {
			currentUpwardsSpeed = JUMP_POWER;
			isInAir = true;
		}
	}

	private void checkInputs() {
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			currentSpeed = RUN_SPEED;
		} else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			currentSpeed = -RUN_SPEED;
		} else {
			currentSpeed = 0;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			currentTurnSpeed = TURN_SPEED;
		} else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			currentTurnSpeed = -TURN_SPEED;
		} else {
			currentTurnSpeed = 0;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			currentSpeed *= 7.0f;
			currentTurnSpeed *= 5.0f;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			jump();
		}
	}

	/**
	 * @return Gets the username of the player.
	 */
	public String getUsername() {
		return username;
	}

	@Override
	public void update(double delta) {
		// System.out.println(username + ": " + super.getEntity().getAABB().toString());
	}
}
