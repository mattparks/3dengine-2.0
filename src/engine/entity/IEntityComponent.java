package engine.entity;

/**
 * Base class for all components that can be attached to entities.
 */
public abstract class IEntityComponent {
	private Entity entity;
	private int id;

	/**
	 * Creates a component attached to a specific entity.
	 *
	 * @param entity The entity this component is attached to.
	 * @param id The id identifying the type of component. This should be unique to the subclass, but not unique to the object.
	 */
	public IEntityComponent(Entity entity, int id) {
		this.id = id;
		this.entity = entity;
		entity.addComponent(this);
	}

	/**
	 * Gets the id of this component.
	 *
	 * @return The id of this component.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the entity this is attached to.
	 *
	 * @return The entity this is attached to.
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * Updates this component.
	 *
	 * @param delta How much time has passed since the last update.
	 */
	public abstract void update(double delta);
}
