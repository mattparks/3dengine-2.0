package engine.factory;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import engine.loader.Loader;
import engine.textures.GameTexture;

/**
 * A factory for creating textures.
 */
public class TextureFactory {
	private static String filePath;
	private static Map<String, SoftReference<GameTexture>> loaded;

	public TextureFactory(String filePath) {
		TextureFactory.filePath = filePath;
		TextureFactory.loaded = new HashMap<>();
	}

	public static GameTexture get(String fileName) {
		return get(fileName, false, false, 1);
	}

	/**
	 * Gets a new object from the factory. If the desired texture data is already loaded, then the texture data is reused and not loaded again. Otherwise, the texture data is loaded from the specified file.
	 *
	 * @param fileName The file path for image file.
	 * @param hasTransparency Does this image contain any transparency?
	 * @param useFakeLighting Should the image be rendered with 'fake' normals? This fixes weird shadowing in transparent components.
	 * @param numberOfRows How many rows each side of image has (use 1 if this image contains only 1 index).
	 * @return Returns the newly created texture object.
	 */
	public static GameTexture get(String fileName, boolean hasTransparency, boolean useFakeLighting, int numberOfRows) {
		fileName = filePath + fileName;
		SoftReference<GameTexture> ref = loaded.get(fileName);
		GameTexture data = ref == null ? null : ref.get();

		if (data == null) {
			loaded.remove(fileName);
			data = new GameTexture(Loader.loadTexture(fileName), hasTransparency, useFakeLighting, numberOfRows);
			loaded.put(fileName, new SoftReference<GameTexture>(data));
		}

		return data;
	}

	public static String getFilePath() {
		return filePath;
	}
}
