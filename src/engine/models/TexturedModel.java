package engine.models;

import engine.textures.GameTexture;

public class TexturedModel {
	private RawModel rawModel;
	private GameTexture texture;

	public TexturedModel(RawModel rawModel, GameTexture texture) {
		this.rawModel = rawModel;
		this.texture = texture;
	}

	public RawModel getRawModel() {
		return rawModel;
	}

	public GameTexture getTexture() {
		return texture;
	}
}
