package game.entities;

import java.io.IOException;

import org.lwjgl.util.vector.Vector3f;

import engine.entity.Entity;
import engine.entity.components.ModelComponent;
import engine.factory.TexturedModelFactory;
import engine.space.ISpatialStructure;

public class EntityRocks extends Entity {
	public EntityRocks(ISpatialStructure<Entity> structure, Vector3f position, Vector3f rotation, float scale) throws IOException {
		super(structure, position, rotation, scale, null);
		new ModelComponent(this, TexturedModelFactory.get("rocks", "rocks"));
	}
}
